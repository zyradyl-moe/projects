## Project Listing

Here you can find a listing of all the various projects I have worked on over
time along with a small blurb about them and any licensing information. This
page fulfills my goal about being more transparent with my projects.
