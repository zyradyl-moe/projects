---
title: NeoBabel
tags: ["hobby", "roleplaying", "creative"]
---

[NeoBabel][1] is a system agnostic hard sci-fi world that I am creating for
tabletop roleplaying games. It could also be used for novels or other forms of
media. It is under heavy development as I first begin putting ideas down on
paper, and is one of my only purely creative pursuits at this time.

The website itself is based on the Hugo static site generator, and the entire
content of the wiki is released under Creative Commons Zero.

**Project Name:** NeoBabel <br />
**Project URL:** https://neobabel.zyradyl.moe <br />
**Project License:** [Creative Commons Zero][2] <br />
**Project Language:** N/A <br />

[1]: https://neobabel.zyradyl.moe/
[2]: https://creativecommons.org/publicdomain/zero/1.0/
