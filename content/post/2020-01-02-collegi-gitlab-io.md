---
title: collegi.gitlab.io
tags: ["ruby", "html", "css", "minecraft", "hobby"]
---

[collegi.gitlab.io][7] is the [GitLab][1] repository for the main website of the
former Collegi Minecraft server. Based on [Jekyll][2], the site is built
through a [Ruby][3] pipeline and utilizes [HTML][4] and [CSS][5].

Several modifications were made to the initial theme to help to repurpose the
blogging platform into a suitable main website.

**Project Name:** collegi.gitlab.io <br />
**Project URL:** https://collegi.gitlab.io <br />
**Project License:** [UNLICENSE][6] <br />
**Project Language:** [Ruby][3], [HTML][4], [CSS][5] <br />

[1]: https://gitlab.com/
[2]: https://jekyllrb.com/
[3]: https://www.ruby-lang.org/en/
[4]: https://en.wikipedia.org/wiki/HTML
[5]: https://en.wikipedia.org/wiki/Cascading_Style_Sheets
[6]: https://unlicense.org/
[7]: https://collegi.gitlab.io/
